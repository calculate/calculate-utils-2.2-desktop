#-*- coding: utf-8 -*-

# Copyright 2010 Calculate Ltd. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
from cl_desktop import desktop, __app__, __version__
from cl_opt import opt
import sys
from cl_share_cmd import share_cmd

# Перевод сообщений для программы
from cl_lang import lang
lang().setLanguage(sys.modules[__name__])

# Использование программы
USAGE = _("%prog [options] user")

# Коментарии к использованию программы
COMMENT_EXAMPLES = _("Create the home directory for user_name")

# Пример использования программы
EXAMPLES = _("%prog user_name")

# Описание программы (что делает программа)
DESCRIPTION = _("Creates the home directory for the new user account")

# Опции командной строки
CMD_OPTIONS = [{'longOption':"verbose",
                'help':_("display the applied template")},
               {'longOption':"set"},
               {'longOption':"install",
                'help':_("install the package")},
               {'longOption':"uninstall",
                'help':_("uninstall the package")},
               {'longOption':"progress",
                'help':_("show progress bar at xdm startup")}]

class desktop_cmd(share_cmd):
    def __init__(self):
        # Объект опций командной строки
        setpos = \
            filter(lambda x:x[1].get('longOption')=="set",
            enumerate(CMD_OPTIONS))[0][0]
        CMD_OPTIONS[setpos] = opt.variable_set[0]
        self.optobj = opt(\
            package=__app__,
            version=__version__,
            usage=USAGE,
            examples=EXAMPLES,
            comment_examples=COMMENT_EXAMPLES,
            description=DESCRIPTION,
            option_list=CMD_OPTIONS + opt.variable_view+opt.color_control,
            check_values=self.checkOpts)
        # Создаем объект логики
        self.logicObj = desktop()
        # Создаем переменные
        self.logicObj.createClVars()
        # Названия несовместимых опций
        self.optionsNamesIncompatible = ["install", "uninstall"]

    def getIncompatibleOptions(self, optObj):
        """Получаем несовместимые опции"""
        retList = []
        for nameOpt in self.optionsNamesIncompatible:
            retList.append(getattr(optObj, nameOpt))
        return retList

    def _getNamesAllSetOptions(self):
        """Выдает словарь измененных опций"""
        setOptDict = self.optobj.values.__dict__.items()
        defaultOptDict = self.optobj.get_default_values().__dict__.items()
        return dict(set(setOptDict) - set(defaultOptDict)).keys()

    def getStringIncompatibleOptions(self):
        """Форматированная строка несовместимых опций разделенных ','"""
        listOpt = list(set(self.optionsNamesIncompatible) &\
                       set(self._getNamesAllSetOptions()))
        return ", ".join(map(lambda x: len(x) == 1 and "'-%s'"%x or "'--%s'"%x,\
                             listOpt))

    def checkOpts(self, optObj, args):
        """Проверка опций командной строки"""
        # Несовместимые опции
        if len(filter(lambda x: x, self.getIncompatibleOptions(optObj)))>1:
                errMsg = _("incompatible options")+":"+" %s"\
                            %self.getStringIncompatibleOptions()
                self.optobj.error(errMsg)
                return False
        if optObj.v or optObj.filter or optObj.xml:
            if args:
                if len(args)>1:
                    errMsg = _("incorrect argument")+":" + " %s" %" ".join(args)
                    self.optobj.error(errMsg)
                    return False
                userName = args[0]
                # Проверка на существование пользователя
                if not self.logicObj.existsUser(userName):
                    return False
        elif optObj.install or optObj.uninstall:
            if args:
                errMsg = _("invalid argument") + ":" + " %s" %" ".join(args)
                self.optobj.error(errMsg)
                return False
        else:
            if not args:
                errMsg = _("no such argument")+":"+" %s" %USAGE.split(" ")[-1]
                self.optobj.error(errMsg)
                return False
            if len(args)>1:
                errMsg = _("incorrect argument") + ":" + " %s" %" ".join(args)
                self.optobj.error(errMsg)
                return False
        if not optObj.v:
            if optObj.filter:
                errMsg = _("incorrect option") + ":" + " %s" %"--filter" +\
                ": " + _("use with option '-v'")
                self.optobj.error(errMsg)
                return False
            if optObj.xml:
                errMsg = _("incorrect option") + ":" + " %s" %"--xml" +\
                ": " + _("use with option '-v'")
                self.optobj.error(errMsg)
                return False
        return optObj, args

    def setUserName(self, userName):
        """Установка имени пользователя"""
        # Проверка на существование пользователя
        if not self.logicObj.existsUser(userName):
            return False
        self.logicObj.clVars.Set("ur_login", userName, True)
        return True

    def createHome(self, optObj):
        """Создание домашней директории"""
        return self.logicObj.createHome(optObj.progress, optObj.verbose)

    def install(self):
        """Инсталяция программы"""
        return self.logicObj.installProg()

    def uninstall(self):
        """Удаление программы"""
        return self.logicObj.uninstallProg()
