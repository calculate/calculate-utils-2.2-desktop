#-*- coding: utf-8 -*-

# Copyright 2010 Calculate Ltd. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

#Допустимые ключи 
#   mode -     режим переменной r-не переназначается из командной строки,
#              w-переназначается из командной строки
#   type     - тип переменной состоит из двух элементов(что это и для чего
#              это)
#   value    - значение переменной по умолчанию
#   hide - флаг того, что данная переменная служебная и не отображается
#              при печати списка значений переменных

from cl_desktop import __version__
from cl_desktop import __app__

class Data:
    # имя программы
    cl_name = {'value':__app__}

    # версия программы
    cl_ver = {'value':__version__}

    #Логин пользователя
    ur_login = {'mode':"r"}

    #Название группы пользователя
    ur_group = {'mode':"r"}

    #Полное имя пользователя
    ur_fullname = {'mode':"r"}

    # Jabber ID пользователя
    ur_jid = {'mode':"r"}

    # Почтовый адрес пользователя
    ur_mail = {'mode':"r"}

    # Домашняя директория пользователя
    ur_home_path = {'mode':"r"}

    # Host Jabber пользователя
    ur_jid_host = {'mode':"w"}

    # переключатель для шаблонов merge
    ac_desktop_merge = {}

    # переключатель для шаблонов инсталяции и удаления программы
    ac_desktop_install = {}

    # переключатель для шаблонов создания пользовательского профиля
    ac_desktop_desktop = {}

    # доменный пользователь "on", "off"
    ur_domain_set = {}

    # user current X session
    cl_desktop_xsession = {}

    # GStreamer data
    cl_desktop_gst_data = {'hide':True}

    # internal card name for xfce mixer
    cl_desktop_gst_card = {}

    # master track name
    cl_desktop_gst_masterchannel = {}

    # list of channel for xfce-perchannel mixer
    cl_desktop_xfce_mixer = {}

    # lib vars
    cl_env_path = {}
    cl_root_path = {'mode':"w"}
    hr_laptop = {}
    hr_laptop_model = {}
    hr_virtual = {}
    os_linux_name = {}
    os_linux_shortname = {}
    os_linux_subname = {}
    os_linux_ver = {}
    os_locale_lang = {}
    os_locale_language = {}
    os_locale_xkb = {}
    os_locale_xkbname = {}
    os_net_hostname = {'mode':"w"}
    os_root_type = {}
    os_x11_composite = {}
    os_x11_height = {'mode':"w"}
    os_x11_standart = {}
    os_x11_video_drv = {}
    os_x11_width = {'mode':"w"}
