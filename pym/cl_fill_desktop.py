#-*- coding: utf-8 -*-

# Copyright 2010 Calculate Ltd. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import re
import os
import sys


class fillVars():
    """Методы определения значений переменных шаблона"""

    def getUserDataInFile(self, login, filePasswd):
        return filter(lambda x: x[0]==login,
                        map(lambda x: x.split(':'),
                            map(lambda x: x.strip(),
                                open(filePasswd).readlines())))

    def get_ur_jid_host(self):
        """Host Jabber пользователя"""
        userJid = self.Get("ur_jid")
        if userJid:
            return userJid.partition('@')[2]
        return ""

    def get_ac_desktop_install(self):
        """переключатель для шаблонов инсталяции и удаления программы"""
        ret = ""
        action = self.Get("cl_action")
        if action in ("install","merge"):
            ret = "up"
        elif action == "uninstall":
            ret = "down"
        return ret

    def get_ac_desktop_merge(self):
        """переключатель для шаблонов merge"""
        ret = ""
        action = self.Get("cl_action")
        if action in ("install","merge"):
            ret = "up"
        return ret

    def get_ac_desktop_desktop(self):
        """переключатель для шаблонов создания пользовательского профиля"""
        ret = ""
        action = self.Get("cl_action")
        if action in ("desktop",):
            ret = "up"
        return ret

    def get_ur_domain_set(self):
        '''доменный пользователь "on", "off"'''
        ret = "off"
        userName = self.Get("ur_login")
        if userName:
            try:
                passwdUserData = self.getUserDataInFile(userName, "/etc/passwd")
            except:
                return ret
            if passwdUserData:
                passwdUserData = passwdUserData[0]
                try:
                    cacheUserData = self.getUserDataInFile(userName,
                        "/var/lib/calculate/calculate-client/cache/passwd")
                except:
                    return ret
                if cacheUserData:
                    cacheUserData = cacheUserData[0]
                    if cacheUserData == passwdUserData:
                        ret = "on"
            else:
                ret = "on"
        return ret

    def get_cl_desktop_xsession(self):
        """Current session"""
        envXsessionFile = "/etc/env.d/90xsession"
        xsession = os.environ.get("XSESSION",None)
        desktopSession = os.environ.get("DESKTOP_SESSION",None)
        if not xsession:
            if os.path.exists(envXsessionFile):
                xsession = \
                    map(lambda x:x.partition("=")[2].strip("'\""),
                    filter(lambda x:x.startswith("XSESSION="),
                    filter(lambda x:not x.startswith("#"),
                    open(envXsessionFile,"r"))))
                if xsession:
                    xsession = xsession[-1]
        if xsession:
            if desktopSession and \
                any(x in desktopSession.lower()
                    for x in ("kde","xfce","gnome")):
                xsession = desktopSession
            if "kde" in xsession.lower():
                return "kde"
            elif "gnome" in xsession.lower():
                return "gnome"
            elif "xfce" in xsession.lower():
                return "xfce"
            else:
                return xsession.lower()
        return ""

    def get_cl_desktop_gst_data(self):
        """GStreamer data"""
        # try import gst
        try:
            olderr = os.dup(sys.stderr.fileno())
            os.close(sys.stderr.fileno())
            import gst
            import gst.interfaces
        except ImportError:
            gst = None
        finally:
            os.dup2(olderr,sys.stderr.fileno())
        if gst is None:
            return {}

        outdata = {}
        try:
            pipeline = "alsamixer"
            alsamixer = gst.element_factory_make(pipeline)
            res = alsamixer.set_state(gst.STATE_PAUSED)
            if res == gst.STATE_CHANGE_SUCCESS:
                outdata['device_name'] = alsamixer.get_property("device-name")
                outdata['long_name'] = alsamixer.get_factory().get_longname()
                outdata['internal_name'] = filter(str.isalnum,
                                "%s (%s)"%(outdata['device_name'],
                                           outdata['long_name']))
                outdata['channels'] = []
                for t in alsamixer.list_tracks():
                    if t.flags & gst.interfaces.MIXER_TRACK_OUTPUT:
                        if t.flags & gst.interfaces.MIXER_TRACK_MASTER or \
                            any(x in t.label 
                                for x in ("Wave","Front","LFE","Center",
                                          "Head","Side","Speaker",
                                          "Surround","PCM")):
                            outdata['channels'].append(t.label)
                            if t.flags & gst.interfaces.MIXER_TRACK_MASTER:
                                outdata['master_channel'] = t.label
        except:
            pass
        return outdata

    def get_cl_desktop_gst_card(self):
        """Internal card name for xfce mixer"""
        return self.Get('cl_desktop_gst_data').get('internal_name','')

    def get_cl_desktop_gst_masterchannel(self):
        """Master track name"""
        return self.Get('cl_desktop_gst_data').get('master_channel','')

    def get_cl_desktop_xfce_mixer(self):
        """List of channel for xfce-perchannel mixer"""
        return "\n".join(
               map(lambda x:'      <value type="string" value="%s" />'%x,
               self.Get('cl_desktop_gst_data').get('channels',[])))
